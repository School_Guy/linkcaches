# How to contribute to this macro?

If you want to help translate this macro in another language just open a PR or write the translated string to my mail:
matrixfueller@gmail.com

Otherwise please keep the 120 character line length limit where sane. Also please comment the code so we know what it
does.

# LinkCaches

The Link to the GSAK-Download-Page: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35197).
The Link to the GSAK-Support-Forum: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35198).

This macro links unlinked caches with just the GC-Number. After that GSAK is able to fetch the rest of the information
via the "Refresh Cache" option in the gc.com menu.